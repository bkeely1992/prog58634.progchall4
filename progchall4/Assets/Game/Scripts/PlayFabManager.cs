﻿using PlayFab;
using PlayFab.ClientModels;
using PlayFab.MultiplayerModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayFabManager : Singleton<PlayFabManager>
{
    public enum LoginState
    {
        Startup,
        Instantiated,
        Success,
        Failed,
    }

    public LoginState state = LoginState.Startup;
    public string playerGUID = "";
    public string playfabID = "";
    public Text userNameText, displayUserNameText, displayPokemonCaughtText, NotificationText;
    public GameObject LoginPanel, PokemonPanel;
    public int numberOfPokemonCaught = 1;

    private void Awake()
    {
        playerGUID = PlayerPrefs.GetString("PlayerGUID", "");
        if (string.IsNullOrEmpty(playerGUID))
        {
            playerGUID = System.Guid.NewGuid().ToString();
            playerGUID = playerGUID.Replace("-", "");
            PlayerPrefs.SetString("PlayerGUID", playerGUID);
        }
    }

    private void Start()
    {
        state = LoginState.Instantiated;

        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest { CustomId = playerGUID, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, onLoginSuccess, onLoginFailure);
    }

    private void onLoginSuccess(LoginResult result)
    {
        state = LoginState.Success;
        Debug.Log("Congratulations, you have logged into PlayFab!!");
        playfabID = result.PlayFabId;

        LocationServicesController.Instance.GetLocation();

        //Check for an existing username:
        PlayFabClientAPI.GetPlayerProfile(
            new GetPlayerProfileRequest
            {
                PlayFabId = playfabID
            },
            new System.Action<GetPlayerProfileResult>(PlayerProfileResultCallback),
            new System.Action<PlayFabError>(PlayerProfileErrorCallback)
        );
    }

    private void onLoginFailure(PlayFabError error)
    {
        state = LoginState.Failed;
        Debug.LogWarning("Something went wront logging into PlayFab");
        Debug.LogError(error.GenerateErrorReport());
    }

    public void updateUserName()
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(
            new UpdateUserTitleDisplayNameRequest() { DisplayName = userNameText.text },
            result =>
            {
                Debug.Log("Successfully updated username.");
                displayUserNameText.text = userNameText.text;
                LoginPanel.SetActive(false);

                //Get the leaderboard value for the player
                PlayFabClientAPI.GetPlayerStatistics
                (
                    new GetPlayerStatisticsRequest
                    {
                        StatisticNames = new List<string>() { "Pokemon Caught" }
                    },
                    new System.Action<GetPlayerStatisticsResult>(GetPokemonCaughtResultCallback),
                    new System.Action<PlayFabError>(GetPokemonCaughtErrorCallback)
                );

            },
            error =>
            {
                Debug.Log("Failed to update username");
            }
        );
    }

    public void catchPokemon()
    {
        //Need lat and lon

        //Check if the pokemon has already been caught
        PlayFabClientAPI.GetUserData(
            new GetUserDataRequest()
            {
                Keys = new List<string>() { PokemonAPIController.Instance.NameText.text },
                PlayFabId = playfabID
            },
            new System.Action<GetUserDataResult>(CheckIfPokemonCaughtResultCallback),
            new System.Action<PlayFabError>(CheckIfPokemonCaughtErrorCallback)
        );
    }

    public void PlayerProfileResultCallback(GetPlayerProfileResult result)
    {
        if(result.PlayerProfile.DisplayName == null)
        {
            LoginPanel.SetActive(true);
        }
        else
        {
            displayUserNameText.text = "Username: " +  result.PlayerProfile.DisplayName;
            LoginPanel.SetActive(false);

            //Get the leaderboard value for the player
            PlayFabClientAPI.GetPlayerStatistics
            (
                new GetPlayerStatisticsRequest
                {
                    StatisticNames = new List<string>() { "Pokemon Caught" }
                },
                new System.Action<GetPlayerStatisticsResult>(GetPokemonCaughtResultCallback),
                new System.Action<PlayFabError>(GetPokemonCaughtErrorCallback)
            );
        }

    }

    public void PlayerProfileErrorCallback(PlayFabError error)
    {
        LoginPanel.SetActive(true);
        Debug.Log("Error loading player profile: " + error.ErrorMessage);
    }

    public void CheckIfPokemonCaughtResultCallback(GetUserDataResult userDataResult)
    {
        if (!userDataResult.Data.ContainsKey(PokemonAPIController.Instance.NameText.text))
        {
            PlayFabClientAPI.UpdatePlayerStatistics(
                new UpdatePlayerStatisticsRequest
                {
                    Statistics = new List<StatisticUpdate>
                    {
                        new StatisticUpdate { StatisticName = "Pokemon Caught", Value = numberOfPokemonCaught }
                    }
                },
                new System.Action<UpdatePlayerStatisticsResult>(UpdatePokemonCaughtResultCallback),
                new System.Action<PlayFabError>(UpdatePokemonCaughtErrorCallback)
            );
        }

        PlayFabClientAPI.UpdateUserData(
            new UpdateUserDataRequest()
            {
                Data = new Dictionary<string, string>()
                {
                    {
                        PokemonAPIController.Instance.NameText.text, LocationServicesController.Instance.LocationText.text
                    }
                }
            },
            result =>
            {
                Debug.Log("Successfully updated pokemon!");
            },
            error =>
            {
                Debug.Log(error.GenerateErrorReport());
            }
        );
    }

    public void CheckIfPokemonCaughtErrorCallback(PlayFabError error)
    {
        Debug.Log("Failed to determine if pokemon was caught.");
    }

    public void UpdatePokemonCaughtResultCallback(UpdatePlayerStatisticsResult result)
    {
        numberOfPokemonCaught++;
        displayPokemonCaughtText.text = "Total Caught: " + numberOfPokemonCaught;
        NotificationText.text = "You just caught a " + PokemonAPIController.Instance.NameText.text;
        Debug.Log("Updated how many pokemone were caught.");
    }

    public void UpdatePokemonCaughtErrorCallback(PlayFabError error)
    {
        Debug.Log("Failed to update how many pokemon have been caught - " + error.ErrorMessage);
    }

    public void GetPokemonCaughtResultCallback(GetPlayerStatisticsResult result)
    {
        if(result.Statistics.Count == 0)
        {
            numberOfPokemonCaught = 0;
        }
        else
        {
            numberOfPokemonCaught = result.Statistics[0].Value;
        }


        displayPokemonCaughtText.text = "Total Caught: " + numberOfPokemonCaught;
        PokemonPanel.SetActive(true);
    }

    public void GetPokemonCaughtErrorCallback(PlayFabError error)
    {
        Debug.Log("Failed to get how many pokemon have been caught.");
    }

}
