﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonAPIController : Singleton<PokemonAPIController>
{
    public Text CurrentPokemonIndexText, NameText;
    public Image PokemonImage;

    [System.Serializable]
    public class Pokemon
    {
        public string name;
        public PokemonSprite sprites;
    }

    [System.Serializable]
    public class PokemonSprite
    {
        public string front_default;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Initialize the image
        OnPokemonIndexChanged("1");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPokemonIndexChanged(string inIndex = "")
    {
        int index;
        if(!int.TryParse(inIndex, out index))
        {
            if (!int.TryParse(CurrentPokemonIndexText.text, out index))
            {
                return;
            }
        }
        string url = string.Format("https://pokeapi.co/api/v2/pokemon/{0}", index);
        HttpWebRequest request = WebRequest.CreateHttp(url);
        HttpWebResponse response = request.GetResponse() as HttpWebResponse;
        StreamReader reader = new StreamReader(response.GetResponseStream());

        string jsonResponse = reader.ReadToEnd();

        reader.Close();
        reader.Dispose();

        Pokemon newPokemon = JsonUtility.FromJson<Pokemon>(jsonResponse);
        NameText.text = newPokemon.name;
        StartCoroutine(GetText(newPokemon.sprites.front_default));
    }

    IEnumerator GetText(string inSite)
    {
        UnityWebRequest www = UnityWebRequest.Get(inSite);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D tex = new Texture2D(2, 2);
            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
            // Load data into the texture.
            tex.LoadImage(results);
            // Assign texture to renderer's material.
            PokemonImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
        }
    }
}
